What is this project ?
----------------------

This project is **NOT** the official VTK repository.

It is a fork of VTK sources hosted at https://gitlab.kitware.com/vtk/vtk.

It is used as staging area to maintain topics specific to VTK that will eventually be contributed back to the official repository.


What is the branch naming convention ?
--------------------------------------

Each branch is named following the pattern `imstk-vX.Y.Z-YYYY-MM-DD-SHA{N}`

where:

* `vX.Y.Z` is the version of VTK
* `YYYY-MM-DD` is the date of the last official commit associated with the branch.
* `SHA{N}` are the first N characters of the last official commit associated with the branch.


What is the title convention for commits ?
------------------------------------------

There are few scenarios to consider:

| Commit status | Title convention |
|--|--|
| already integrated upstream | Amend the commit title to include `[Backport] This is the original title`
| being considider for upstream integration | Amend the commit title to include `[Backport PR-<NNN>] This is the original title` where `<NNN>` identifies the associated pull request being reviewed. |
| applies only to iMSTK | If the commit will not be considered for upstream integration, consider using the prefix `[iMSTK] This is the title` |


Are there exceptions to the branch naming convention ?
------------------------------------------------------

 - customRenderPipeline: Existed before conventions set forth. Contents need to be investigated to figure out if we can discard.

 - multi-textures: Existed before conventions set forth. Contents need to be investigated to figure out if we can discard.